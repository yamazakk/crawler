#coding:UTF-8
import sys
import time
import csv
import re

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

def main():
    """
    メイン処理
    """
    start = time.time()#計測時間
    driver = webdriver.PhantomJS()  # PhantomJSのWebDriverオブジェクトを作成する。
    driver.set_window_size(800, 600)  # ウィンドウサイズを設定する。

    #unextログイン用IDとPASSの設定
    # unextid = ''
    # unextpass = ''
    # loginunext(driver, unextid, unextpass)


    #取得サイトのURLを指定する
    crowlurl = []
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000131') #洋画一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000132') #邦画一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000133') #海外ドラマ一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000134') #韓流・アジアドラマ一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000135') #国内ドラマ一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000136') #アニメ一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000137') #キッズ一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000832') #ドキュメンタリー一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000833') #音楽アイドル一覧
    crowlurl.append('http://video.unext.jp/browse/genre/MNU0000138') #バラエティ一覧


    #ターゲットサイトの配列分、取得処理を行う
    loopcnt = 0 #ヘッダー出力を初回のみにするための変数

    with open('unext_titledata.csv', mode="w") as targetfile:
        for targeturl in crowlurl:
            print(targeturl + 'から映画データを取得します。')
            moviedata = navigate(driver, targeturl)

            # CSV書き込み
            fieldnames = ("title", "playurl", "thumbnailurl")
            header = dict([(val, val)for val in fieldnames])

            # ヘッダー出力は初回のみ
            if loopcnt == 0:
                moviedata.insert(0, header)

            writer = csv.DictWriter(targetfile, fieldnames, quoting=csv.QUOTE_NONNUMERIC)
            writer.writerows(moviedata)

            loopcnt = loopcnt + 1

    #計測時間出力
    elapsed_time = time.time() - start
    print("計測時間:" + str(int((elapsed_time))) + "[sec]")
    print('information:program_complieted')

# def loginunext(driver, hid, hpass):
#     """
#     unextのログインを行う
#     """
    #ログインサイトへ遷移
    # print('unextにログインします。')
    # driver.get('https://secure.unext.jp/login')
    # time.sleep(3)


def existcheck(driver, atr, target):
    """
    class,cssがその画面に存在するかどうかを確認し、TRUEorFALSEを返す
    入力: (driver, 'CLASS' or 'CSS', Class名 or CSS名)
    出力: True or False
    コピペ用：existcheck(driver, 'CLASS', '')
    """
    if  atr == 'CLASS':
        elements = driver.find_elements_by_class_name(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    elif atr == 'CSS':
        elements = driver.find_elements_by_css_selector(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    else:
        print('atr引数は\'CLASS\'か\'CSS\'のどちらかのみ入力可能です。処理を終了します')
        sys.exit()


def navigate(driver, targeturl):
    """
    unextタイトル一覧画面から、映画タイトル・再生URL・画像URLを取得する
    """
    #検索サイトへ遷移
    driver.get(targeturl)
    time.sleep(4)


    loopflg = 0
    while loopflg == 0:
        #スクロール前の要素の位置を取得
        before = driver.execute_script("var y = window.pageYOffset; return y;")
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        print('現在のスクロール要素位置は' + str(before))
        time.sleep(3)

        #ページのスクロールできる一番下までスクロールする。
        driver.execute_script('scroll(0, document.body.scrollHeight)')
        time.sleep(2)

        #スクロール後の要素の位置を取得
        after = driver.execute_script("var y = window.pageYOffset; return y;")

        #座標位置が変更していないのならflgを立てて処理を終了
        if before == after:
            print("座標に変更なし、スクロール処理を終了します")
            loopflg = 1

    #マウスオーバーでタイトルを表示させる。(seleniumマウスオーバーでは１つずつしかタイトル表示できないのでscriptで一括で処理)
    driver.execute_script('$(".ui-item-v__catch").trigger("mouseover")')
    time.sleep(1)

    #タイトル取得
    movietitle = []
    elements_title = driver.find_elements_by_css_selector('div.ui-item-v__name-thumb--list-collection')

    for sto in elements_title:
        #print(sto.text)
        movietitle.append(str(sto.text))

    #再生URL取得
    playurl = []
    elements_playurl = driver.find_elements_by_css_selector('a.ui-item-v__link')

    for sto in elements_playurl:
        #print(sto.get_attribute('href'))
        playurl.append(sto.get_attribute('href'))

    #サブネイルURL取得
    thumbnailurl = []
    elements_thumbnailurl = driver.find_elements_by_class_name('ui-item-v__thumb--list-collection')

    for sto in elements_thumbnailurl:
        #Stringへ変換
        strurl = str(sto.get_attribute('style'))
        # URLを正規表現で抜き出し
        urls = re.search(u'(https?)(:\/\/[-_.!~*\'a-zA-Z0-9;\/?:\@&=+\$,%#]+)', strurl)
        #print(urls.group())
        thumbnailurl.append(urls.group())


    #取得したタイトル・再生URL・サブネイルURLの数が同じか調べる。同じであれば正常
    if not len(movietitle) == len(playurl) == len(thumbnailurl):
        print('*******取得したタイトル・再生URL・画像URLの画像の件数が一致しません。どこかで取得できていないものがあるかも*******')
        print('取得映画タイトル数' + str(len(movietitle)))
        print('取得再生URL数' + str(len(playurl)))
        print('取得画像URL数' + str(len(thumbnailurl)))



    #タイトル・再生url・画像urlのlistを１行でリストにする
    moviedata = []
    for cnt in range(0, len(movietitle)):
        #print(movietitle[cnt] + playurl[cnt] + thumbnailurl[cnt])
        moviedata.append({"title":movietitle[cnt], "playurl":playurl[cnt], "thumbnailurl":thumbnailurl[cnt]})

    return moviedata


if __name__ == '__main__':
    main()

