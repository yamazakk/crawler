#coding:UTF-8
import sys
import time
import csv
import re

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

def main():
    """
    メイン処理
    """
    start = time.time()#計測時間
    driver = webdriver.PhantomJS()  # PhantomJSのWebDriverオブジェクトを作成する。
    driver.set_window_size(800, 600)  # ウィンドウサイズを設定する。


    #タイトルCSV(unext_titledata.csv)の読み込み
    moviedata = []
    with open('unext_titledata.csv', 'r') as rfile:
        reader = csv.reader(rfile)
        next(reader)  # ヘッダーはスキップ

        for row in reader:
            #読み込んだデータ(title,playurl,thumbnailurl)を一行にしてListへ格納
            moviedata.append(row[0]+ ',' + row[1]+ ',' + row[2])


    #Listのループ内でウェブサイトからデータを１つずつ取得していく
    for targetdata in moviedata:

        #1行のlistデータを変数に
        rowdata = targetdata.split(",") #カンマでスライス
        title = rowdata[0]
        print(rowdata[0])
        playurl = rowdata[1]
        thumbnailurl = rowdata[2]

        #navigate(driver, playurl)
        #print(title, playurl, thumbnailurl)


    #計測時間出力
    elapsed_time = time.time() - start
    print("計測時間:" + str(int((elapsed_time))) + "[sec]")
    print('information:program_complieted')

# def loginunext(driver, hid, hpass):
#     """
#     unextのログインを行う
#     """
    #ログインサイトへ遷移
    # print('unextにログインします。')
    # driver.get('https://secure.unext.jp/login')
    # time.sleep(3)


def existcheck(driver, atr, target):
    """
    class,cssがその画面に存在するかどうかを確認し、TRUEorFALSEを返す
    入力: (driver, 'CLASS' or 'CSS', Class名 or CSS名)
    出力: True or False
    コピペ用：existcheck(driver, 'CLASS', '')
    """
    if  atr == 'CLASS':
        elements = driver.find_elements_by_class_name(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    elif atr == 'CSS':
        elements = driver.find_elements_by_css_selector(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    else:
        print('atr引数は\'CLASS\'か\'CSS\'のどちらかのみ入力可能です。処理を終了します')
        sys.exit()


def navigate(driver, targeturl):
    """
    unext指定されs再生URLから映画情報を取得する
    取得するデータ
    ・ストーリー
    (ジャンル)
    ・発表年月
    ・発表国
    ・金額(無料or有料)
    
    ・キャスト（役名なし・出演者名のみ）
    ・スタッフ
    """
    #検索サイトへ遷移
    driver.get(targeturl)
    time.sleep(4)


    #タイトル取得
    movietitle = []
    elements_title = driver.find_elements_by_css_selector('div.ui-item-v__name-thumb--list-collection')

    for sto in elements_title:
        #print(sto.text)
        movietitle.append(str(sto.text))

    #再生URL取得
    playurl = []
    elements_playurl = driver.find_elements_by_css_selector('a.ui-item-v__link')

    for sto in elements_playurl:
        #print(sto.get_attribute('href'))
        playurl.append(sto.get_attribute('href'))

    #サブネイルURL取得
    thumbnailurl = []
    elements_thumbnailurl = driver.find_elements_by_class_name('ui-item-v__thumb--list-collection')

    for sto in elements_thumbnailurl:
        #Stringへ変換
        strurl = str(sto.get_attribute('style'))
        # URLを正規表現で抜き出し
        urls = re.search(u'(https?)(:\/\/[-_.!~*\'a-zA-Z0-9;\/?:\@&=+\$,%#]+)', strurl)
        #print(urls.group())
        thumbnailurl.append(urls.group())


    #取得したタイトル・再生URL・サブネイルURLの数が同じか調べる。同じであれば正常
    if not len(movietitle) == len(playurl) == len(thumbnailurl):
        print('*******取得したタイトル・再生URL・画像URLの画像の件数が一致しません。どこかで取得できていないものがあるかも*******')
        print('取得映画タイトル数' + str(len(movietitle)))
        print('取得再生URL数' + str(len(playurl)))
        print('取得画像URL数' + str(len(thumbnailurl)))



    #タイトル・再生url・画像urlのlistを１行でリストにする
    moviedata = []
    for cnt in range(0, len(movietitle)):
        #print(movietitle[cnt] + playurl[cnt] + thumbnailurl[cnt])
        moviedata.append({"title":movietitle[cnt], "playurl":playurl[cnt], "thumbnailurl":thumbnailurl[cnt]})

    return moviedata


if __name__ == '__main__':
    main()

