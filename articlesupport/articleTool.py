# coding:utf-8

# シネマトゥデイデータ取得スクリプト(スクレイピング正規表現版)
import os
import sys
import time
import re
import datetime
from dateutil.relativedelta import relativedelta
import MySQLdb


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from apiclient.discovery import build  # $ pip install --upgrade google-api-python-client
from MySQLdb.cursors import DictCursor



def main():
    """
    メインの処理。
    """
    start = time.time()#計測時間
    driver = webdriver.PhantomJS()  # PhantomJSのWebDriverオブジェクトを作成する。
    driver.set_window_size(800, 600)  # ウィンドウサイズを設定する。


    # 対象のタイトルとURLの取得(検索するdict)
    dict_gettilte_urls = navigate_get_title(driver) #ディクショナリで取得ーtitle(タイトル) val([0]発表年月, [1]URL)

    #SQL取得ーリスト(検索されるlist)
    list_dbtitledata = dbinput_titledata()

    #重複チェック-対象タイトルの選別
    #辞書キー(タイトル)からDBタイトルリストを検索。なければターゲットリストに追加
    dict_targets_datas = {} #詳細情報を取得する対象タイトルとURLを格納するディクショナリ

    #cnt = 0 #テスト用

    for key_title, val in dict_gettilte_urls.items():
        # if cnt == 3: #テスト用
        #     break
        if key_title in list_dbtitledata:
            pass
        else:
            print(key_title + 'は存在しません。追加対象にします')
            dict_targets_datas[key_title] = val

        #cnt += 1 #テスト用
    print('追加対象は ' + str(len(dict_targets_datas)) + '件です。')

    #映画詳細情報の取得(１件ずつ取得する)
    list_outmoviedatas = []
    for title, val in dict_targets_datas.items():
        print(title + 'の詳細情報を取得します。')

        #詳細情報取得
        list_moviedatas = [title, val[0], val[1]] #val[0]=公開年月 val[0]=対象URL
        list_contentdata = getcarwldata(driver, val[1]) #コンテンツ取得処理(1件)
        list_moviedatas.extend(getscrapingdata(list_contentdata[0])) #スクレイピング処理(1件)
        list_moviedatas.append(get_embedurl_youtube(title)) #youtube情報を追加
        list_moviedatas.append(list_contentdata[1]) #ポスター画像URLを追加
        list_moviedatas.append(list_contentdata[0]) #スクレイピング処理前のデータを追加
        #print(list_moviedatas)
        #配列準備
        list_outmoviedatas.append(list_moviedatas)

    #print(list_outmoviedatas)
    dbinsert(list_outmoviedatas) #DB挿入処理

    #計測時間出力
    elapsed_time = time.time() - start
    print ("計測時間:" + str(int((elapsed_time))) + "[sec]")



def dbinput_titledata():
    """
    DBの現在のタイトルを全て取得してリストで返却
    """
    USER_KEY = os.environ['mysql_user']
    PASS_KEY = os.environ['mysql_pass']

    # MySQL接続
    con = MySQLdb.connect(
        user   = USER_KEY,
        passwd = PASS_KEY,
        host   = '127.0.0.1',
        db     = 'movieon',
        charset= 'utf8'
        )

    # カーソルを取得する
    cur = con.cursor(DictCursor)

    query = "SELECT title FROM tb_autoarticle;"
    cur.execute(query)

    # 実行結果をすべて取得する
    rows = cur.fetchall()

    titledata = [] #返却用

    # タイトルデータをリストに格納
    for row in rows:
        titledata.append(row['title'])


    return titledata



def navigate_get_title(driver):
    """
    シネマトゥデイの当月と翌月のタイトルと詳細URLを取得する
    """

    #取得する月とその翌月分を対象とする。URLが年月となっているので、年月を取得
    today = datetime.datetime.today() # 今日の日付を取得
    next_month = today + relativedelta(months=1) # 翌月を取得

    # URL生成(取得URLの形式-http://www.cinematoday.jp/movie/release/201704)
    current_month_url = 'http://www.cinematoday.jp/movie/release/' + today.strftime('%Y%m')
    next_month_url = 'http://www.cinematoday.jp/movie/release/' + next_month.strftime('%Y%m')

    #当月のタイトル取得処理
    print('当月の対象映画情報を取得します')
    driver.get(current_month_url)
    time.sleep(3)

    #公開タイトルと詳細画面URLの取得
    title_element = driver.find_elements_by_css_selector\
    ('div.col-main ol#movie-ranking li div.detail a')

    #ディクショナリに映画タイトルと詳細URL格納
    movietitle = {}

    for ele in title_element:
        movietitle[ele.text] = today.strftime('%Y%m'), ele.get_attribute('href')

    print(str(len(title_element)) + '件検出しました' )

    #翌月のタイトル取得処理
    print('翌月の対象映画情報を取得します')
    driver.get(next_month_url)
    time.sleep(3)

    #公開タイトルと詳細画面URLの取得
    title_element = driver.find_elements_by_css_selector\
    ('div.col-main ol#movie-ranking li div.detail a')

    #ディクショナリに映画タイトルと詳細URL格納
    for ele in title_element:
        movietitle[ele.text] = next_month.strftime('%Y%m'), ele.get_attribute('href')

    print(str(len(title_element)) + '件検出しました' )

    return movietitle



def getcarwldata(driver, targetirl):
    """
    コンテンツ取得
    """
    driver.get(targetirl)
    time.sleep(3)

    #本文の一括取得
    content = driver.find_element_by_css_selector\
    ('html body#ct-std.ct-pc div#ct-container div#contents div.col-main')

    #ポスターURL
    poster_elements = driver.find_elements_by_css_selector\
    ('div#contents div.col-main div.box div#opt_images ul li div a.cboxElement')

    posturls = []

    #リスト化
    for posterurl_ele in  poster_elements:
        playurl = posterurl_ele.get_attribute('href')
        posturls.append(playurl)

    #リスト化したものをjoinで１つの文字列
    posturls = ', '.join(posturls)

    return [content.text, posturls] #listで返却



def getscrapingdata(contentdata):
    """
    取得したデータを整形する
    """
    #対象
    highlights = ''
    story = ''
    official_url = ''
    staff = ''
    cast = ''
    releasedate = ''
    showtime = ''
    createcompany = ''


    #print(contentdata) #全文表示
    print('*****************************')

    text = contentdata

    # パターン(正規表現)宣言
    pt_midokoro = r"見どころ：.*"
    pt_arasuzi = r"あらすじ：.*"
    pt_officialurl = r"公式サイト：.*"
    pt_staff = r"スタッフ\n.*[\s\S]*(?=キャスト)"
    pt_cast = r"キャスト\n.*[\s\S]*(?=ブログ)"
    pt_releasedate = r"日本公開：\n.*[\s\S]"
    pt_showtime = r"上映時間：\n.*[\s\S]"

    #パターンのコンパイル
    rept_midokoro = re.compile(pt_midokoro) #みどころ
    rept_arasuzi = re.compile(pt_arasuzi) #あらすじ
    rept_officialurl = re.compile(pt_officialurl) #公式サイト
    rept_staff = re.compile(pt_staff) #スタッフ
    rept_cast = re.compile(pt_cast) #キャスト
    rept_releasedate = re.compile(pt_releasedate) #日本公開
    rept_showtime = re.compile(pt_showtime) #上映時間

    #マッチング処理
    match_midokoro = rept_midokoro.search(text)
    match_arasuzi = rept_arasuzi.search(text)
    match_officialurl = rept_officialurl.search(text)
    match_staff = rept_staff.search(text)
    match_cast = rept_cast.search(text)
    match_releasedate = rept_releasedate.search(text)
    match_showtime = rept_showtime.search(text)


    #出力処理

    #みどころ
    if match_midokoro:
        # print('【見どころ出力】')
        # print(re.sub('見どころ：', '', match_midokoro.group())) #表題(見どころ:)を削除
        highlights = re.sub('見どころ：', '', match_midokoro.group())

    #あらすじ
    if match_arasuzi:
        # print('【あらすじ出力】')
        # print(re.sub('あらすじ：', '', match_arasuzi.group())) #表題(あらすじ:)を削除
        story = re.sub('あらすじ：', '', match_arasuzi.group())

    #公式サイト
    if match_officialurl:
        # print('【公式サイト出力】')
        # print(re.sub('公式サイト：', '', match_officialurl.group())) #表題(公式サイト:)を削除
        official_url = re.sub('公式サイト：', '', match_officialurl.group())

    #スタッフ
    if match_staff:
        # print('【スタッフ出力】')
        staff = re.sub('スタッフ\n', '', match_staff.group())
        #print(re.sub('スタッフ\n', '', match_staff.group())) #表題(スタッフ:)を削除
        # print(match_staff.group())
        
    else:
        pt_staff = r"スタッフ\n.*[\s\S]*(?=ブログ)"
        rept_staff = re.compile(pt_staff) #スタッフ
        match_staff = rept_staff.search(text)
        if match_staff:
            staff = re.sub('スタッフ\n', '', match_staff.group())

    #キャスト
    if match_cast:
        # print('【キャスト出力】')
        #print(re.sub('キャスト\n', '', match_cast.group())) #表題(キャスト:)を削除
        # print(match_cast.group())
        cast = re.sub('キャスト\n', '', match_cast.group())

    #日本公開日
    if match_releasedate:
        # print('【日本公開出力】')
        # print(re.sub('日本公開：\n', '', match_releasedate.group())) #表題(制作会社:\n)を削除
        releasedate = re.sub('日本公開：\n', '', match_releasedate.group())
        releasedate = releasedate.rstrip("\n") #末尾改行除去

    #上映時間
    if match_showtime:
        # print('【上映時間出力】')
        # print(re.sub('上映時間：\n', '', match_showtime.group())) #表題(上映時間:\n)を削除
        showtime = re.sub('上映時間：\n', '', match_showtime.group())
        showtime = showtime.rstrip("\n") #末尾改行除去


    #リストに格納して(固定行)返す
    list_moviedata = []
    list_moviedata = [highlights, story, official_url, staff, cast, \
                      releasedate, showtime]

    return list_moviedata


def get_embedurl_youtube(query):
    """
    youtubeの埋め込み動画を取得する
    """
    youtube_api_key = 'AIzaSyCLeKkD5WbNZ_0DcY9m6-9FWc4iCz3S0Ac'
    youtube = build('youtube', 'v3', developerKey=youtube_api_key)  # YouTubeのAPIクライアントを組み立てる。
    # search.listメソッドで最初のページを取得するためのリクエストを得る。
    search_response = youtube.search().list(
        part='snippet',
        q=query,
        type='video',
    ).execute()

    # search_responseはAPIのレスポンスのJSONをパースしたdict。
    embedurls = []
    for item in search_response['items']:
        embedurl_txt = '<iframe width="560" height="315" src="https://www.youtube.com/embed/' \
                        + item['id']['videoId'] + '" frameborder="0" allowfullscreen></iframe>'

        embedurls.append(embedurl_txt)
        #print(item['snippet']['title'])#動画のタイトルを表示する。
        #print(item['id']['videoId']) # 動画のタイトルを表示する。

    #配列化したものを文字列に
    embedurls = embedurls = ', '.join(embedurls)

    return embedurls



def dbinsert(outmoviedatas):
    """
    DBにリストのデータを追加する
    リストのデータは全て追加対象となる
    """
    # MySQL接続
    con = MySQLdb.connect(
        user='root',
        passwd='taisuke0719333',
        host='127.0.0.1',
        charset='utf8',
        db='movieon')

    # カーソルを取得する
    cur = con.cursor()


    for datas in outmoviedatas:
        # print('【1:タイトル】' + datas[0])
        # print('【2:シネマトゥデイ公開年月】' + datas[1])
        # print('【3:シネマトゥデイURL】' + datas[2])
        # print('【4:見所・】' + datas[3])
        # print('【5:ストーリー】' + datas[4])
        # print('【6:公式サイト】' + datas[5])

        # print('【7:スタッフ】' + datas[6])
        # print('【8:キャスト】' + datas[7])

        # print('【9:日本公開日】' + datas[8])
        # print('【10:上映時間】' + datas[9])
        # print('【11:Youtube埋め込みタグ】' + datas[10])
        # print('【12:ポスター画像URL】' + datas[11])
        # print('【13:スクレイピング処理前データ】' + datas[12])

        QUERY = "INSERT INTO tb_autoarticle \
        ( title, cinamatoday_date, cinematoday_url, highlights, story, official_url, staff, cast, roaddate, showtime, embedurl, pic_url, before_content) \
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"

        quedata = str(datas[0]), \
                  str(datas[1]), \
                  str(datas[2]), \
                  str(datas[3]), \
                  str(datas[4]), \
                  str(datas[5]), \
                  str(datas[6]), \
                  str(datas[7]), \
                  str(datas[8]), \
                  str(datas[9]), \
                  str(datas[10]), \
                  str(datas[11]), \
                  str(datas[12])

        cur.execute(QUERY, quedata)


    #SQLコミット
    con.commit()

    cur.close
    con.close



if __name__ == '__main__':
    main()

