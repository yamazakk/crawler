#coding:UTF-8
import sys
import time
import csv

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

def main():
    """
    メインの処理。
    """
    start = time.time()#計測時間
    driver = webdriver.PhantomJS()  # PhantomJSのWebDriverオブジェクトを作成する。
    driver.set_window_size(800, 600)  # ウィンドウサイズを設定する。

    #CSVの読み込み(取得タイトルファイル)
    print('CSVを読み込みます。')
    title_names = [] #タイトル読み込み用配列
    csvcnt = 0
    fi = open('hulu_title_data.csv', 'r')
    dataReader = csv.reader(fi)
    for row in dataReader:
        title_names.append(row[0])
        csvcnt = csvcnt + 1

    print('CSVを読み込みました。読み込みタイトル数は' + str(csvcnt) + '件です')

    #CSVの書き込み準備(映画データファイル)
    fo = open('hulu_data.csv', 'a')
    dataWriter = csv.writer(fo)

    #CSVの書き込み準備(エラーログファイル)ー取得できなかったタイトルを書き出す
    fr = open('hulu_error_title.csv', 'w')
    errorWriter = csv.writer(fr)

    #huluログイン用IDとPASSの設定
    huluid = 'movieeestr@gmail.com'
    hulupass = 'taisuke8841'
    loginhulu(driver, huluid, hulupass)

    #読み込んだCSVのタイトルを取得する
    for searchtitle in title_names:
        try:
            result = navigate(driver,searchtitle) # データ取得処理
            print (result)
            dataWriter.writerow(result)
            csvcnt = csvcnt - 1
            print('残り件数は' + str(csvcnt) + '件です。')
        except:
            print('＊＊＊＊＊＊＊＊例外を捕捉しました。スキップします＊＊＊＊＊＊＊＊')
            errorWriter.writerow(str(searchtitle))
    #moviedatacheck() #取得
    #moviedataout() #取得データをMysqlに出力処理


    #計測時間出力
    elapsed_time = time.time() - start
    print ("計測時間:" + str(int((elapsed_time))) + "[sec]")

def loginhulu(driver, hid, hpass):
    """
    huluのログインを行う
    """
    #ログインサイトへ遷移
    print('huluにログインします。')
    driver.get('https://secure.hulu.jp/login')
    time.sleep(3)
    element = driver.find_element_by_name("login")
    element.send_keys(hid)
    element = driver.find_element_by_name("password")
    element.send_keys(hpass)
    element.submit()
    time.sleep(3)
    print('ログインしました。')

def existcheck(driver, atr, target):
    """
    class,cssがその画面に存在するかどうかを確認し、TRUEorFALSEを返す
    入力: (driver, 'CLASS' or 'CSS', Class名 or CSS名)
    出力: True or False
    コピペ用：existcheck(driver, 'CLASS', '')
    """
    if  atr == 'CLASS':
        elements = driver.find_elements_by_class_name(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    elif atr == 'CSS':
        elements = driver.find_elements_by_css_selector(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    else:
        print('atr引数は\'CLASS\'か\'CSS\'のどちらかのみ入力可能です。処理を終了します')
        sys.exit()


def navigate(driver, searchword):
    """
    hulu検索、映画データを取得する
    """
    #検索サイトへ遷移
    driver.get('http://www.hulu.jp/browse/movies')
    time.sleep(4)

    #検索フォームに検索ワード入力&Submit
    print('■■■■■■■■■■【' + searchword + '】を検索します■■■■■■■■■■')
    element = driver.find_element_by_name("q")
    element.send_keys(searchword)
    element.submit()
    time.sleep(2)
    #検索画面後映画ボタンをクリックする。
    driver.find_element_by_xpath('/html/body/div[3]/div[4]/div[1]/div/div/div[4]/div[2]/div[1]/table/tbody/tr/td[3]/div/div/a[3]').click()
    time.sleep(2)
    driver.save_screenshot("映画ボタンクリック後スクショ.png")


    #データ取得処理
    #ターゲットの要素取得
    elements  = driver.find_elements_by_class_name('thumbnail')

    #マウスオーバー
    ActionChains(driver).move_to_element(elements[1]).perform()
    time.sleep(3)

    #Story取得(descripton)/詳細ボタンが付いていたら動画再生画面
    story = driver.find_element_by_css_selector('html body.small div#hover-box.z-index-layer-6 div.hover-info.movie div.description').text
    #Storyの中に'詳細'があれば、'詳細'の文字列を削除する。全文は取得しない。
    if '詳細' in story :  
        story = story.replace('詳細', '')

        #print('ストーリー:'+ story)
    else:
        pass
        #print('ストーリー:'+ story)
        

    #ジャンル・コピーライト・上映時間・発表年月・字幕・配給会社を取得
    #再生URL取得(映画によっては再生URLではないこともある)
    playurlelement = driver.find_element_by_css_selector('div.thumbnail a.beacon-click')
    playurl = playurlelement.get_attribute('href')
    #print('再生URL:'+ playurl)

    #サブネイル画像URL取得
    picurlelement = driver.find_element_by_css_selector('div.thumbnail a.beacon-click img.thumbnail')
    picurl = picurlelement.get_attribute('src')
    #print('サブネイル画像URL:'+ picurl)
    
    #ジャンル取得
    #div.hover-info.show div.genre a
    if existcheck(driver,'CSS','div.hover-info.movie div.genre a'):
        genre = driver.find_element_by_css_selector('div.hover-info.movie div.genre a').text
        #print('ジャンル:'+ genre)
    elif existcheck(driver,'CSS','div.hover-info.show div.genre a'):
        genre = driver.find_element_by_css_selector('div.hover-info.show div.genre a').text
        #print('ジャンル:'+ genre)

    #Copyright取得
    if existcheck(driver,'CSS','div.hover-info.movie div.art-copyright'):
        copyright = driver.find_element_by_css_selector('div.hover-info.movie div.art-copyright').text
        #print('コピーライト:'+ copyright)
    elif existcheck(driver,'CSS','div.hover-info.show div.art-copyright'):
        copyright = driver.find_element_by_css_selector('div.hover-info.show div.art-copyright').text      
        #print('コピーライト:'+ copyright)

    #上映時間(season)
    if existcheck(driver,'CSS','div.hover-info.movie div.season'):
        runningtime = driver.find_element_by_css_selector('div.hover-info.movie div.season').text
        #print('上映時間:'+ runningtime)

    #発表年月(details)
    if existcheck(driver,'CSS','div.hover-info.movie div.details span'):
        announce = driver.find_element_by_css_selector('div.hover-info.movie div.details span').text
        #print('発表年月:'+ announce)

    #playurlが再生画面でなければ追加データ取得
    #追加データはplayurlの変更・story全文取得・genre全取得・配給会社取得
    if 'watch' not in playurl:
        print('詳細画面があります。詳細情報を取得します。')
        driver.get(playurl)
        time.sleep(3)

        #playurlを張り替え
        playurlelement = driver.find_element_by_css_selector('div.thumbnail.play-hover a.beacon-click')
        playurl = playurlelement.get_attribute('href')
        #print('再生URL:'+ playurl)

        #ストーリー'全文'を取得する
        #ストーリー前文取得
        story  = driver.find_element_by_class_name('text-truncate').text
               
        #ストーリー後文があれば、詳細ボタンをクリックしてから取得
        if existcheck(driver,'CSS','div.details-info-static span.text-break a.blue-link.more'):
            clickelement = driver.find_element_by_css_selector('div.details-info-static span.text-break a.blue-link.more')
            clickelement.click()
            time.sleep(1)
        if existcheck(driver,'CSS','div.details-info-static span.text-rest'):    
            storyafter = driver.find_element_by_css_selector('div.details-info-static span.text-rest').text
            story = story + storyafter
        #print('ストーリー:'+ story)

        #ジャンル全取得
        genre = driver.find_element_by_class_name('genres').text
        #print('ジャンル:'+ genre)

        #配給会社を取得
        company = driver.find_element_by_class_name('company').text
        #print('配給会社:'+ company)
        
        #ホバー用ターゲットの要素取得
        elements  = driver.find_elements_by_class_name('description')
        #マウスオーバー
        ActionChains(driver).move_to_element(elements[1]).perform()
        time.sleep(3)
        #配給会社
        company = driver.find_element_by_css_selector('div.hover-info.movie div.season').text
        #print('配給会社:'+ company)       

    return searchword,story,genre,runningtime,announce,playurl,picurl
    #スクリーンショット
    driver.save_screenshot("スクショ.png")

if __name__ == '__main__':
    main()

