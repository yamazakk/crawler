# -*- coding: utf-8 -*-

# fuluのサイトから映画情報を全て抜き出す
require 'anemone'
require 'open-uri'
require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'
require 'benchmark'

Capybara.current_driver = :selenium
Capybara.default_wait_time = 5



module CrawlerFulu
	class MovieFulu
			include Capybara::DSL
        

            # ログイン処理
            def login(id,pass)
                visit('http://www.hulu.jp/browse/movies')
                find("a.text.login").click
                sleep(1)
                puts "ログインします。"
                page.execute_script "$('#login').val('#{id}')"
                page.execute_script "$('#password').val('#{pass}')"
                find(".btn-login").click
                puts "ログインしました。"
                sleep(3)
            end

			# 情報取得処理
			def search
                #　書き込みするファイルをオープン
                csv = File.open('test.csv',"w")
                csv.print("\"タイトル\",\"ストーリー\",\"ジャンル\",\"ジャンル2\",\"コピーライト\",\"上映時間\",\"発表年\",\"字幕\",\"配給会社\"\n")
 
                File.foreach('hulu_firstdata.csv') do |searchword|
                    @searchword = searchword.chomp
                    puts "（検索ワード）#{@searchword}"
                    visit('http://www.hulu.jp/browse/movies')
                    # 検索フォームに検索ワード入力
                    page.execute_script "$('#q').val('#{@searchword}')"
                    # 検索ボタンクリック
                    find("#search-icon").click
                    sleep(2)
                    # 詳細情報を抜き出し

                    #ストーリー情報を抜き出し
                    #　検索先にストーリー説明があるかどうか判断、なければ映画詳細画面へ
                    if page.has_selector?('span.description') == true
                        #映画画像にマウスホバー(映画情報を表示させる)
                        page.execute_script '$(".play-hover").trigger("mouseover")'
                        sleep(3)
                        story = page.all("span.description")[0].text
                    else
                        #詳細画面に遷移
                        page.all(".thumbnail")[0].click
                        sleep(5)

                        #詳細ボタンがある場合はクリック
                        if page.has_selector?('a.blue-link.more') == true
                            page.all("a.blue-link.more")[0].click
                            sleep(1)
                            story  = find(".text-truncate").text
                            #詳細ボタンがあっても、続きのストーリー文章がないケースもあるので、対策
                            if page.has_selector?('.text-rest') == true
                                story2  = find(".text-rest").text
                                story   = story << story2
                            end
                            
                        else
                        #詳細ボタンがない場合
                            story  = find(".text-truncate").text
                        end
                        #配給会社取得処理
                        if page.has_selector?('.company') == true
                            company      = find(".company").text
                        end
                        #ジャンル取得処理
                        if page.has_selector?('a.blue-link.beacon.beacon-click') == true

                            allgenre = Array.new
                            allgenre = []
                            find_all("a.blue-link.beacon.beacon-click").each do |title|
                                allgenre.push(title.text)
                            end
                            genre2 = allgenre.join(",")
                        end
                        
                    end
                    # ストーリー以外の情報を取得(ホバーから取得する情報)
                    page.execute_script '$(".play-hover").trigger("mouseover")'
                    # ジャンル
                    if page.has_selector?('.genre') == true
                        genre      = find(".genre").text
                    else
                        genre      = "データなし"
                    end
                    #ジャンル2
                    if genre2.nil?
                        genre2 = "データなし"
                    end 
                    # コピーライト
                    if page.has_selector?('.art-copyright') == true
                        copyright  = find(".art-copyright").text
                    else
                        copyright  = "データなし"
                    end
                    # 上映時間
                    if page.has_selector?('.season') == true
                        time       = find(".season").text
                    else
                        time       = "データなし"
                    end
                    # 発表年月
                    if page.has_selector?('.details') == true
                        age        = find(".details").text
                    else
                        age        = "データなし"
                        #発表年のデータがない場合は、copyrightに年月があったらそれを加工して代入する。
                    end   
                    # 字幕
                    if page.has_selector?('.captions') == true
                        captions   = find(".captions").text
                    else
                        captions   = "データなし"              
                    end    
                    #　配給会社
                    if company.nil?
                        company = "データなし"
                    end 
                    puts "【ストーリー】"  + story
                    puts "【ジャンル】"    + genre
                    puts "【ジャンル2】"   + genre2
                    puts "【コピーライト】" + copyright
                    puts "【上映時間】"    + time
                    puts "【発表年月】"   + age
                    puts "【字幕】"      + captions
                    puts "【配給会社】"   +company

                    csv.print("\"#{@searchword}\",\"#{story}\",\"#{genre}\",\"#{genre2}\",\"#{copyright}\",\"#{time}\",\"#{age}\",\"#{captions}\",\"#{company}\"\n")
                end
            end
	end
end

#**********************************************************************************
# main
#**********************************************************************************
##fuluからメインキャストで検索・関連タイトル取得
result = Benchmark.realtime do
    crawler = CrawlerFulu::MovieFulu.new
    #$searchword = "バイオハザード"
    crawler.login("movieeestr@gmail.com","taisuke8841")
    crawler.search
end
puts "全体処理時間 #{result}s"
# title = crawler.report


