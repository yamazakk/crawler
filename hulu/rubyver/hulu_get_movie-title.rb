# -*- coding: utf-8 -*-

# fuluのサイトから映画情報を全て抜き出す
require 'anemone'
require 'open-uri'
require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'
require 'benchmark'

Capybara.current_driver = :selenium
Capybara.default_wait_time = 5

module CrawlerFulu
	class MovieFulu
			include Capybara::DSL
        

			# 遷移のための処理
			def gettitle
				visit('http://www.hulu.jp/browse/movies')

                #画面下までスクロール
                result = Benchmark.realtime do
                    #映画出力用の配列
                    alltitle = Array.new
                    alltitle = []

                	#自動スクロール処理、Javascriptで見えない、要素を全て出力させる。
                	loop{

                 		#スクロール前の要素の位置を取得
                		$before = page.execute_script("var y = window.pageYOffset; return y;")
                		page.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                		sleep(3)

                        # JavaScriptで映画の上にホバーして、タイトル名を表示させる。
                        page.execute_script '$(".thumbnail").trigger("mouseover")'
                        sleep(3)

                        #タイトル名を書き出し
                        scrolltitle = Array.new
                        scrolltitle = []
                        find_all(".title").each do |title|
                            #scrolltitle.push(title.text)
                            alltitle.push(title.text)
                        end     
                        sleep(1)

                		#スクロール後の要素の位置を取得
                		$after = page.execute_script("var y = window.pageYOffset; return y;")
                		
                        #座標位置が変更していないのなら処理を終了
                		if $before == $after then
                			puts "座標に変更なし、スクロール処理を終了します。"
                			#正常flg
                			break
                		end
                	}
                    puts alltitle.uniq
                end
                puts "全体処理時間 #{result}s"

				sleep(3)
			end


			# テキスト抽出処理
			def report
				#配列を宣言
				alltitle = Array.new
				alltitle = []
				#タイトル名を書き出し
				find_all(".title").each do |title|
					alltitle.push(title.text)
					#puts title.text
				end
				sleep(3)
				return alltitle
			end
	end
end

#**********************************************************************************
# main
#**********************************************************************************
##fuluからメインキャストで検索・関連タイトル取得
crawler = CrawlerFulu::MovieFulu.new
crawler.gettitle
# title = crawler.report


