import sys
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

def main():
    """
    メインの処理。
    """
    driver = webdriver.PhantomJS()  # PhantomJSのWebDriverオブジェクトを作成する。
    driver.set_window_size(800, 600)  # ウィンドウサイズを設定する。

    #huluログイン用IDとPASSの設定
    huluid = 'movieeestr@gmail.com'
    hulupass = 'taisuke8841'
    loginhulu(driver, huluid, hulupass)
    navigate(driver,searchword)# データ取得処理
    #moviedatacheck() #取得

def loginhulu(driver, hid, hpass):
    """
    huluのログインを行う
    """
    #ログインサイトへ遷移
    print('huluにログインします。')
    driver.get('https://secure.hulu.jp/login')
    time.sleep(3)
    element = driver.find_element_by_name("login")
    element.send_keys(hid)
    element = driver.find_element_by_name("password")
    element.send_keys(hpass)
    element.submit()
    time.sleep(3)
    driver.save_screenshot("ログイン画面のスクショ.png")
    print('ログインしました。')

def existcheck(driver, atr, target):
    """
    class,cssがその画面に存在するかどうかを確認し、TRUEorFALSEを返す
    入力: (driver, 'CLASS' or 'CSS', Class名 or CSS名)
    出力: True or False
    コピペ用：existcheck(driver, 'CLASS', '')
    """
    if  atr == 'CLASS':
        elements  = driver.find_elements_by_class_name(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    elif atr == 'CSS':
        elements  = driver.find_elements_by_css_selector(target)
        if len(elements) > 0:
            #print('見つかった要素数:'+ str(len(elements)))
            return True
        else:
            #print('見つかった要素数:'+ str(len(elements)))
            return False
    else:
        print('atr引数は\'CLASS\'か\'CSS\'のどちらかのみ入力可能です。処理を終了します')
        sys.exit()


def navigate(driver,searchword):
    """
    hulu検索、映画データを取得する
    """
    #検索サイトへ遷移
    driver.get('http://www.hulu.jp/browse/movies')
    time.sleep(4)

    #検索フォームに検索ワード入力&Submit
    print('■■■■■■■■■■【' + searchword + '】を検索します■■■■■■■■■■')
    element = driver.find_element_by_name("q")
    element.send_keys(searchword)
    element.submit()
    time.sleep(2)
    #検索画面後映画ボタンをクリックする。
    driver.find_element_by_xpath('/html/body/div[3]/div[4]/div[1]/div/div/div[4]/div[2]/div[1]/table/tbody/tr/td[3]/div/div/a[3]').click()
    time.sleep(2)
    driver.save_screenshot("映画ボタンクリック後スクショ.png")


    #検索結果の画面構成パターンを判別する
    if existcheck(driver,'CLASS','play-hover'):
        if existcheck(driver,'CLASS','div.promo-show-container.movie.two-part'):
            pattern = 1
        else:
            pattern = 2
    else:
        pattern = 4


    #データ取得処理
     
    if pattern == 1:# 検索結果パターン1
        print('パターン1で処理します')
        #ターゲットの要素取得
        elements  = driver.find_elements_by_class_name('play-hover')

        #マウスオーバー
        ActionChains(driver).move_to_element(elements[0]).perform()
        time.sleep(3)

        #ストーリー取得
        story = driver.find_element_by_css_selector('div.description span.description').text
        print('ストーリー:'+ story)
        

    elif pattern == 2:# 検索結果パターン2
        print('パターン2で処理します')
        
        #ターゲットの要素取得
        elements  = driver.find_elements_by_css_selector('div.thumbnail a.promo-title-link.beacon-click')
        #マウスオーバー
        ActionChains(driver).move_to_element(elements[0]).perform()
        time.sleep(3)
        
        #ストーリー取得
        if existcheck(driver,'CSS','div.promo-show-description div.description span.description'):
            story = driver.find_element_by_css_selector('div.promo-show-description div.description span.description').text
            print('ストーリー:'+ story)


    elif pattern == 3:# 検索結果パターン3
        pass

    elif pattern == 4:# 検索結果パターン4(サブネイルのみ表示される場合)
        print('パターン4で処理します')
        #ターゲットの要素取得
        #div.thumbnail a.beacon-click img.thumbnail
        elements  = driver.find_elements_by_class_name('thumbnail')

        #マウスオーバー
        ActionChains(driver).move_to_element(elements[1]).perform()
        time.sleep(3)

        #Story取得(descripton)/詳細ボタンが付いていたら動画再生画面
        story = driver.find_element_by_css_selector('html body.small div#hover-box.z-index-layer-6 div.hover-info.movie div.description').text
        #Storyの中に'詳細'があれば、'詳細'の文字列を削除する。全文は取得しない。
        if '詳細' in story :  
            story = story.replace('詳細', '')
            # #詳細再生画面へ遷移
            # print('詳細再生画面へ遷移します')
            # driver.find_element_by_xpath('/html/body/div[8]/div[1]/div[4]/a').click()
            # time.sleep(2)
            # #ストーリー取得
            # story  = driver.find_element_by_class_name('div.video-details.toggle-panel div.description').text

            print('ストーリー:'+ story)
        else:
            print('ストーリー:'+ story)
        
        #再生URLor詳細URLを取得(この時点では判別できない)>再生URLに代入
        
    else:
        print('エラー!パターンに該当しません。')
    

    #ジャンル・コピーライト・上映時間・発表年月・字幕・配給会社を取得
    #再生URL取得(映画によっては再生URLではないこともある)
    playurlelement = driver.find_element_by_css_selector('div.thumbnail a.beacon-click')
    playurl = playurlelement.get_attribute('href')
    print('再生URL:'+ playurl)

    #サブネイル画像URL取得
    picurlelement = driver.find_element_by_css_selector('div.thumbnail a.beacon-click img.thumbnail')
    picurl = picurlelement.get_attribute('src')
    print('サブネイル画像URL:'+ picurl)
    
    #ジャンル取得
    #div.hover-info.show div.genre a
    if existcheck(driver,'CSS','div.hover-info.movie div.genre a'):
        genre = driver.find_element_by_css_selector('div.hover-info.movie div.genre a').text
        print('ジャンル:'+ genre)
    elif existcheck(driver,'CSS','div.hover-info.show div.genre a'):
        genre = driver.find_element_by_css_selector('div.hover-info.show div.genre a').text
        print('ジャンル:'+ genre)

    #Copyright取得
    if existcheck(driver,'CSS','div.hover-info.movie div.art-copyright'):
        copyright = driver.find_element_by_css_selector('div.hover-info.movie div.art-copyright').text
        print('コピーライト:'+ copyright)
    elif existcheck(driver,'CSS','div.hover-info.show div.art-copyright'):
        copyright = driver.find_element_by_css_selector('div.hover-info.show div.art-copyright').text      
        print('コピーライト:'+ copyright)

    #上映時間(season)
    if existcheck(driver,'CSS','div.hover-info.movie div.season'):
        runningtime = driver.find_element_by_css_selector('div.hover-info.movie div.season').text
        print('上映時間:'+ runningtime)

    #発表年月(details)
    if existcheck(driver,'CSS','div.hover-info.movie div.details span'):
        announce = driver.find_element_by_css_selector('div.hover-info.movie div.details span').text
        print('発表年月:'+ announce)

    #playurlが再生画面でなければ追加データ取得
    #追加データはplayurlの変更・story全文取得・genre全取得・配給会社取得
    if 'watch' not in playurl:
        print('詳細画面があります。詳細情報を取得します。')
        driver.get(playurl)
        time.sleep(3)

        #playurlを張り替え
        playurlelement = driver.find_element_by_css_selector('div.thumbnail.play-hover a.beacon-click')
        playurl = playurlelement.get_attribute('href')
        print('再生URL:'+ playurl)

        #ストーリー'全文'を取得する
        #ストーリー前文取得
        story  = driver.find_element_by_class_name('text-truncate').text
               
        #ストーリー後文があれば、詳細ボタンをクリックしてから取得
        if existcheck(driver,'CSS','div.details-info-static span.text-break a.blue-link.more'):
            clickelement = driver.find_element_by_css_selector('div.details-info-static span.text-break a.blue-link.more')
            clickelement.click()
            time.sleep(1)
        if existcheck(driver,'CSS','div.details-info-static span.text-rest'):    
            storyafter = driver.find_element_by_css_selector('div.details-info-static span.text-rest').text
            story = story + storyafter
        print('ストーリー:'+ story)

        #ジャンル全取得
        genre = driver.find_element_by_class_name('genres').text
        print('ジャンル:'+ genre)

        #配給会社を取得
        company = driver.find_element_by_class_name('company').text
        print('配給会社:'+ company)
        
        #コピーライト取得
        copyright = driver.find_element_by_class_name('copy-right-details').text
        print('コピーライト:'+ copyright)
        
        #ホバー用ターゲットの要素取得
        elements  = driver.find_elements_by_class_name('description')
        #マウスオーバー
        ActionChains(driver).move_to_element(elements[1]).perform()
        time.sleep(3)
        #発表年月
        company = driver.find_element_by_css_selector('div.hover-info.movie div.season').text
        print('配給会社:'+ company)       

    #スクリーンショット
    driver.save_screenshot("最後のスクショ.png")

if __name__ == '__main__':
    main()

