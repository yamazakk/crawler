# -*- coding: utf-8 -*-
# huluの最新取り扱い動画（映画・テレビ番組）のタイトルを取得する。
# 対象URLは２つ
# 映画
# テレビ番組
import sys
import time
import csv
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

def main():
    """
    メインの処理。
    """
    start = time.time()#計測時間
    driver = webdriver.PhantomJS()  # PhantomJSのWebDriverオブジェクトを作成する。
    driver.set_window_size(800, 600)  # ウィンドウサイズを設定する。

    #テレビ番組タイトル取得
    hulu_get_televisiontitle()
    #映画タイトル取得
    hulu_get_movietitle()

    #計測時間出力
    elapsed_time = time.time() - start
    print ("計測時間:" + str(int((elapsed_time))) + "[sec]")

def loginhulu(driver, hid, hpass):
    """
    huluのログインを行う
    """
    #ログインサイトへ遷移
    print('huluにログインします。')
    driver.get('https://secure.hulu.jp/login')
    time.sleep(3)
    element = driver.find_element_by_name("login")
    element.send_keys(hid)
    element = driver.find_element_by_name("password")
    element.send_keys(hpass)
    element.submit()
    time.sleep(3)
    driver.save_screenshot("ログイン画面のスクショ.png")
    print('ログインしました。')


def hulu_get_televisiontitle():
    """
    huluからテレビ番組のタイトルを取得する
    """
    driver.get('http://www.hulu.jp/browse/movies')
    
def hulu_get_movietitle():
    """
    huluから映画のタイトルを取得する
    """
    driver.get('http://www.hulu.jp/browse/movies')

if __name__ == '__main__':
    main()
